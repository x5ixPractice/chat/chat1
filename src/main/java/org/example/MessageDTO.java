package org.example;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.example.model.Message;

@Getter@Setter
@NoArgsConstructor
public class MessageDTO {

    private String text;
    private String datetime;
    private String username;

    public static MessageDTO mapToDTO(Message message){
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setDatetime(message.getDateTime().toString());
        messageDTO.setText(message.getMessage());
        messageDTO.setUsername(message.getUser().getName());
        return messageDTO;
    }

}
