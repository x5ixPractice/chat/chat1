package org.example;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.example.model.Message;
import org.example.model.MessageRepository;
import org.example.model.User;
import org.example.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.condition.RequestConditionHolder;

import java.time.LocalDateTime;
import java.util.*;

@RestController
@AllArgsConstructor
public class ChatController {

    @Autowired
    private final UserRepository userRepository;
    @Autowired
    private final MessageRepository messageRepository;

    @GetMapping("/init")
    public HashMap<String, Boolean> init() {
        HashMap<String, Boolean> response = new HashMap<>();
        String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
        Optional<User> userOpt = userRepository.findBySessionId(sessionId);

        response.put("result", userOpt.isPresent());
        return response;
    }

    @PostMapping("/auth")
    public HashMap<String, Boolean> auth(@RequestParam String name) {
        HashMap<String, Boolean> response = new HashMap<>();
        String sessionId = RequestContextHolder.currentRequestAttributes().getSessionId();

        User user = new User();
        user.setName(name);
        user.setSessionId(sessionId);
        userRepository.save(user);

        response.put("result", true);
        return response;
    }

    @PostMapping("/message")
    public Map<String, Boolean> sendMessage(@RequestParam String message) {
        Map<String, Boolean> response = new HashMap<>();
        if (message.isEmpty()) return Map.of("Empty message", false);

        String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
        Optional<User> userOptional = userRepository.findBySessionId(sessionId);
        if (userOptional.isPresent() == false)
            return Map.of("Error! User wasn't found!", false);

        Message messageObject = new Message();
        messageObject.setMessage(message);
        messageObject.setDateTime(LocalDateTime.now());
        messageObject.setUser(userOptional.get());
        messageRepository.save(messageObject);

        response.put("result", userOptional.isPresent());
        return response;
    }

    @GetMapping("/message")
    public List<MessageDTO> getMessagesList() {
        return messageRepository
                .findAll(Sort.by(Sort.Direction.ASC, "dateTime"))
                .stream()
                .map(message -> MessageDTO.mapToDTO(message))
                .toList();
    }

    @GetMapping("/user")
    public List<User> getUsersList() {

        return userRepository.findAll();
    }
}
